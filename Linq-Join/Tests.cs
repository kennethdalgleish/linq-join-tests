﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Linq_Join
{
    public class Tests
    {
        public static int Counter = 0;

        public void Run(Action<IEnumerable<TestClass>, IEnumerable<TestClass>> function)
        {
            Counter = 0;
            var innerCollection = TestClass.Generate(10000);
            var outerCollection = TestClass.Generate(10000);

            var stopWatch = Stopwatch.StartNew();
            function(innerCollection, outerCollection);
            Console.WriteLine($"{function.Method.Name} Elapsed in {stopWatch.ElapsedMilliseconds}ms. Get count: {Counter}");
        }

        public static void EnumerableTest(IEnumerable<TestClass> innerCollection, IEnumerable<TestClass> outerCollection)
        {
            var result = innerCollection.Select(inner => outerCollection.FirstOrDefault(outer => outer.Key.Equals(inner.Key))).ToList();
        }

        public static void ListTest(IEnumerable<TestClass> innerCollection, IEnumerable<TestClass> outerCollection)
        {
            var inneEnumerated = innerCollection.ToList();
            var outerEnumerated = outerCollection.ToList();

            var result = inneEnumerated.Select(inner => outerEnumerated.FirstOrDefault(outer => outer.Key.Equals(inner.Key))).ToList();
        }

        public static void DictionaryTest(IEnumerable<TestClass> innerCollection, IEnumerable<TestClass> outerCollection)
        {
            var outerDictionary = outerCollection.ToDictionary(x => x.Key);
            var result = innerCollection.Select(inner => outerDictionary.TryGetValue(inner.Key, out var bpd) ? bpd : null).ToList();
        }

        public static void JoinTest(IEnumerable<TestClass> innerCollection, IEnumerable<TestClass> outerCollection)
        {

            var result = from inner in innerCollection
                join outer in outerCollection on inner.Key equals outer.Key
                select inner;

            result = result.ToList();
        }
    }
}