﻿using System.Collections.Generic;
using System.Linq;

namespace Linq_Join
{
    public class TestClass
    {
        private readonly string _key;

        public static IEnumerable<TestClass> Generate(int numberofItems)
        {
            var resultList = new List<TestClass>();
            for (var i = 1; i < numberofItems; i++)
            {
                resultList.Add(new TestClass(i));
            }

            resultList.Shuffle();

            return resultList.AsEnumerable();
        }

        public TestClass(int key)
        {
            _key = key.ToString();
        }

        public string Key
        {
            get
            {
                Tests.Counter++;
                return _key;
            }
        }
    }
}