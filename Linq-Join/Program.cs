﻿using System;

namespace Linq_Join
{
    class Program
    {
        static void Main(string[] args)
        {
            var tests = new Tests();
            tests.Run(Tests.EnumerableTest);
            tests.Run(Tests.EnumerableTest);
            tests.Run(Tests.ListTest);
            tests.Run(Tests.JoinTest);
            tests.Run(Tests.DictionaryTest);
            Console.ReadKey();
        }
    }
}


